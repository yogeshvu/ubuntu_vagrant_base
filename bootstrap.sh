#!/usr/bin/env bash

hello_func() {
	echo "Hello World"
	sudo apt-get update
}

#install git core
#sudo apt-get install git-core -y

#sudo apt-get install ubuntu-desktop -y
#sudo apt-get install lightdm -y

#This installs the java
java8_func(){
	sudo add-apt-repository ppa:webupd8team/java
	sudo apt-get update
	sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
	sudo apt-get install oracle-java8-installer -y
	sudo apt-get install oracle-java8-set-default -y
}

java6_func(){
    echo "installing java6"
    oracle-java6-installer
    sudo echo oracle-java6-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections
    sudo apt-get install oracle-java6-installer -y
    sudo apt-get install oracle-java6-set-default -y
    echo "Current Java Version is: "
    java -version
}

eclipse_func(){
    echo "Now Installing the eclipse"
    cd /home/vagrant/Downloads/
    wget http://mirror.cc.vt.edu/pub/eclipse/technology/epp/downloads/release/luna/SR2/eclipse-java-luna-SR2-linux-gtk-x86_64.tar.gz
    tar -xvf eclipse.tar.gz -C ../eclipse 
}

# This Installs the sublime-text-3
sublime3_func(){
	sudo add-apt-repository ppa:webupd8team/sublime-text-3
	sudo apt-get update
	sudo apt-get install sublime-text-installer -y
}


#Finally Prepare this Box for package
emptybox_func(){
	sudo apt-get clean -y
	sudo dd if=/dev/zero of=/EMPTY bs=1M
	sudo rm -f /EMPTY
}

#lastly clear the bash_history and quit
historyclear_func(){
	echo "Cleaning the history"
	cat /dev/null > ~/.bash_history && history -c 
}
#Finally shutdown the system
shutdown_func(){
	echo "System Shutting Down"
	sudo shutdown now -h
}


hello_func
#historyclear_func
#shutdown_func

java6_func
eclipse_func